<h1>Baby's Last Feeding</h1>

<div class="last-feeding">
  <div class="last-feeding-info">
    <!--<span class="time">{{ lastFed | date: 'hh:mm' }}<span>{{ lastFed | date: 'a' }}</span></span>
    <span class="date">{{ lastFed | date: 'mediumDate' }}</span>
    <span class="amount">{{ amount }} oz.</span>-->
  </div>
  <img src="/images/last-background.svg" alt="" class="last-feeding-background">
</div>

<a href="#/add" class="button button-block">Add Feeding</a>

<div class="history"><a href="#/history"><img src="/images/history.svg" alt=""> View History</a></div>