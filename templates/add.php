<h1>Add Feeding</h1>

<form>

  <ul>
    <li>
      <label>Time:</label>
      <input type="text" ng-model="setTime" name="time">
    </li>
    <li>
      <label>Date:</label>
      <input type="text" ng-model="setDate" name="date">
    </li>
    <li>
      <label>Amount:</label>
      <input type="number" ng-model="setAmount" name="amount">
    </li>
  </ul>

  <button class="button button-block" ng-click="sendFeeding()">Save Feeding</button>
  <p class="control-back"><a href="#/">Cancel</a></p>
</form>
