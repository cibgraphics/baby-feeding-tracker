<p class="control-back"><a href="#/">Back</a></p>

<h1>History</h1>

<div>
  <div class="history" ng-repeat="feeding in listing">
    <h2 class="text-right">{{ feeding.date | date: "mediumDate" }}</h2>
    <ul>
      <li ng-repeat="info in feeding.feeding">
        <a href="#/history">
          <span class="time">{{ info.time | date: 'h:mm a' }}</span>
          <span class="amount">{{ info.amount }} oz.</span>
        </a>
      </li>
    </ul>
  </div>
</div>