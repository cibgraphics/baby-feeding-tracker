module.exports = function(grunt) {

  // Make sure when installing plugins to use 'npm install <module> --save-dev' to have it add automatically to package.json
  // When installing from a already setup project, use 'npm install' to install dependencies
  // To uninstall dependencies, use npm uninstall <module>

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Add plugin tasks

    //Watch Files
    watch: {
      options: {
        livereload: true
      },
      scripts: {
        files: ['js/**/*.js'],
        tasks: [],
        options: {
          spawn: false
        }
      },
      css: {
        files: ['css/less/**/*.less'],
        tasks: ['less'],
        options: {
          spawn: false
        }
      },
      html:{
        files: ['./**/*.html', './**/*.php'],
        tasks: [],
        options: {
            spawn: false
        }
      }
    },

    // Compile Less
    less: {
      development: {
        options: {
          compress: false
        },
        files: {
          "css/style.css": "css/less/style.less"
        }
      }
    }

  });

  // Load the plugin
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');

  // Default task(s).
  grunt.registerTask('default', ['watch']);
};