// Routes

app.config(function ($routeProvider) {

    $routeProvider

    .when('/', {
        templateUrl: 'templates/main.php',
        controller: 'mainController'
    })

    .when('/add', {
        templateUrl: 'templates/add.php',
        controller: 'mainController'
    })

    .when('/history', {
        templateUrl: 'templates/history.php',
        controller: 'mainController'
    })

});