app.controller('mainController', ['$scope', '$http', '$filter', function($scope, $http, $filter) {

  var currentDate = new Date();

  $scope.setTime = $filter('date')(currentDate, 'h:mm a');
  $scope.setDate = $filter('date')(currentDate, 'MMM d yyyy');
  $scope.setAmount = '';


  $scope.lastFed = currentDate;
  $scope.amount = "12";


  $scope.sendFeeding = function() {
    $http.post('ajax/add-feedings.php', {
      'time' : $scope.setTime,
      'date' : $scope.setDate,
      'amount' : $scope.setAmount
    })
      .success(function(feeding) {
        window.location.replace("/");
      })
      .error(function(data) {
        alert('There was a problem sending the data.');
      })
  };

  $http.get('ajax/get-feedings.php')
    .success(function(data) {
      $scope.listing = [];

      for (var i = 0; i < data.length; i += 1) {
        $scope.listing.push({
          date: new Date(data[i].date),
          feeding: data[i].feeding
        });
      }
    })
    .error(function(data){
       alert('There was a problem with the feed.');
    })

}]);


/*
angular.module('app').constant('angularMomentConfig', {
    timezone: 'America/Denver' // optional
});

app.controller('LastFeedingController', function($scope) {
  $scope.lastFed = new Date();
  $scope.amount = "12";
});

app.controller('FeedingHistoryControler', function($scope) {
  $scope.feedings = [
    {
      date: '2014-09-04',
      feeding: [
        {
          id: '1',
          time: '1409852728000',
          amount: '3'
        },
        {
          id: '2',
          time: '1409874328000',
          amount: '4'
        },
      ]
    },
    {
      date: '2014-09-05',
      feeding: [
        {
          id: '3',
          time: '1409915908000',
          amount: '3.5'
        },
        {
          id: '4',
          time: '1409957908000',
          amount: '5'
        },
      ]
    },
  ]
});

app.controller('PullFeedingControler', function($scope, $http) {

  $http.get('ajax/get-feedings.php')
    .success(function(data) {
      $scope.listing = [];

      for (var i = 0; i < data.length; i += 1) {
        $scope.listing.push({
          date: new Date(data[i].date),
          feeding: data[i].feeding
        });
      }
      console.log($scope.listing);
    })
    .error(function(data){
       alert('There was a problem with the feed.');
    })
});


app.controller('AddFeedingControler', function($scope, $http) {

  $scope.sendFeeding = function() {
    $http.post('ajax/add-feedings.php', {
      'time' : $scope.feeding.time,
      'date' : $scope.feeding.date,
      'amount' : $scope.feeding.amount
    })
      .success(function(feeding) {
        window.location.replace("/");
      })
      .error(function(data) {
        alert('There was a problem sending the data.');
      })
  }

});*/