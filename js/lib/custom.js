$(function() {

  var RB = new RubberBand(function(e) {
     // make your call to AJAX or simply reload the page
     location.reload();
     // don't forget to close RubberBand when you're done.
     e.close();
   });

  $(document).on('click', 'a', function(e) {
    e.preventDefault();
    document.location.href = $(this).attr('href');
  });

});