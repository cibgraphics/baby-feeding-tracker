
      </section><!-- End .content -->

    </div><!-- End .container -->

    <!-- jQuery & Angular -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.9/angular.min.js"></script>
    <script src="/js/lib/angular-route.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.9/angular-resource.js"></script>

    <!-- Custom JS -->
    <script src="/js/lib/rubberband.js"></script>
    <script src="/js/lib/moment.js"></script>
    <script src="/js/lib/moment-timezone.js"></script>
    <script src="/js/lib/angular-moment.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/lib/angular/routes.js"></script>
    <script src="/js/lib/angular/services.js"></script>
    <script src="/js/lib/angular/controllers.js"></script>
    <script src="/js/lib/angular/directives.js"></script>
    <script src="/js/lib/custom.js"></script>
  </body>
</html>