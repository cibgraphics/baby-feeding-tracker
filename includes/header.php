<?php
  $currentDate = date('M n, Y');
  $currentTime = date('g:i A');
  $defaultAmount = '6';

  date_default_timezone_set('America/Denver');

  //echo $currentTime;
  //.echo date_default_timezone_get()
?>

<!DOCTYPE html>
  <html lang="en-US" dir="ltr" ng-app="app">

  <head>
    <meta charset="utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <title>Vanilla5 Document</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <meta name="format-detection" content="telephone=no" /><!-- Use <a href="tel:xxx-xxxx"> to use for mobile  -->

    <!-- HTML5 Shiv for IE -->
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <!-- Stylesheets -->
    <link href="/css/style.css" rel="stylesheet" media="screen" />

    <!-- iOS Stuff -->
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link rel="apple-touch-startup-image" href="/startup.png">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon.png">

  </head>

  <body>
    <div id="RubberBandjs">
      <div class="rband">
        <img src="/images/rubberband/down-arrow.png" class="arrow" />
        <img src="/images/rubberband/loading.gif" class="load" />
        <h3 class="text"></h3>
      </div>
    </div>

    <header class="site-masthead clear-fix">
      <div class="color-block pink"></div>
      <div class="color-block green"></div>
      <div class="color-block blue"></div>
      <div class="color-block pink"></div>
      <div class="color-block green"></div>
      <div class="color-block blue"></div>
      <div class="color-block pink"></div>
      <div class="color-block green"></div>
    </header><!-- End site-masthead-->

    <div class="container">

      <section class="content">