<?php

  function fetch_all($table, $sorting, $where = '0 = 0', $start_row = 0, $rows_per_page = 100){
    global $db;
    $sql = "SELECT *
        FROM {$table}
        WHERE {$where}
        ORDER BY {$sorting}
        LIMIT {$start_row},
        {$rows_per_page}";
    $query = @mysql_query($sql, $db);
    if(mysql_num_rows($query) > 0){
      $recordset = array();
      while($row = mysql_fetch_assoc($query)){
        $recordset[] = $row;
      }
      return $recordset;
    } else {
      return false;
    }
  }

  function redirect($location = NULL) {
    if($location != NULL) {
      header("location: {$location}");
    }
  }

   function dateparse($in)
  {
    $in = json_decode($in);
    $out = array();
    for ($i = 0; $i < sizeof($in); $i++) {
        $date = $in[$i]->date;
        $isFound = false;
        for ($i2 = 0; $i2 < sizeof($out); $i2++) {
            if ($date == $out[$i2]["date"]) {
                // We've already run into this search value before
                // So add the the elements
                $isFound = true;
                $out[$i2]["feeding"][] = array(
                    "id" => $in[$i]->id,
                    "time" => $in[$i]->time,
                    "amount" => $in[$i]->amount);
                break;
            }
        }
        if (!$isFound) {
            // No matches yet
            // We need to add this one to the array
            $feeding = array("id" => $in[$i]->id, "time" => date('c', strtotime($in[$i]->date)), "amount" => $in[$i]->amount);
            $out[] = array("date" => date('c', strtotime($in[$i]->date)), "feeding" => array($feeding));
        }
    }
    return json_encode($out);
  }

?>